package main

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"log"
	"time"
)

type Config struct {
	KongAddress             string        `envconfig:"kong_address" default:"http://localhost:8001"`
	KongConnectionTimeout   time.Duration `envconfig:"kong_connection_timeout" default:"10s"`
	KongReadTimeout         time.Duration `envconfig:"kong_read_timeout" default:"1s"`
	SnapshotUpdaterInterval time.Duration `envconfig:"snapshot_updater_interval" default:"5s"`
	XdsServerPort           int           `envconfig:"xds_server_port" default:"40000"`
}

func (c *Config) ToString() string {
	return fmt.Sprintf("Kong address: %v, Kong connection timeout: %v, Kong read timeout: %v"+
		", Snapshot Updater Interval: %v, Xds server port: %v", c.KongAddress, c.KongConnectionTimeout,
		c.KongReadTimeout, c.SnapshotUpdaterInterval, c.XdsServerPort)
}

func ReadConfig() Config {
	var c Config
	err := envconfig.Process("kcp", &c)
	if err != nil {
		log.Fatalf("Error while reading the config: %v", err)
	}
	return c
}
