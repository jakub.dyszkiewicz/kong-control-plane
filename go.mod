module github.com/jakubdyszkiewicz/kong-control-plane

go 1.12

require (
	github.com/envoyproxy/go-control-plane v0.6.9
	github.com/gogo/googleapis v1.1.0 // indirect
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/lyft/protoc-gen-validate v0.0.13 // indirect
	github.com/stretchr/testify v1.3.0
	google.golang.org/grpc v1.19.0
)
