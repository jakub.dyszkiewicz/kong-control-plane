package main // import "github.com/jakubdyszkiewicz/kong-control-plane"

import (
	"github.com/jakubdyszkiewicz/kong-control-plane/envoy"
	"github.com/jakubdyszkiewicz/kong-control-plane/kong"
	"github.com/jakubdyszkiewicz/kong-control-plane/state"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func main() {
	config := ReadConfig()
	log.Printf("Starting kong-control-plane with config: %v", config.ToString())

	stateUpdater := state.Updater{
		Source: &kong.Source{
			Client: kong.NewClient(config.KongAddress, config.KongConnectionTimeout, config.KongReadTimeout),
		},
		Target: &envoy.Target{},
	}

	server := envoy.Server{}

	wg := sync.WaitGroup{}
	wg.Add(2)

	var gracefulStop = make(chan os.Signal, 1)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)

	server.Start(&wg, config.XdsServerPort)
	stateUpdater.Start(&wg, config.SnapshotUpdaterInterval)

	<-gracefulStop
	stateUpdater.Stop()
	server.Stop()
	wg.Wait()
}
