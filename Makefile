build:
	go build -v
test: build
	go test ./...
int-test: build
	./integration-test.sh
run-dependencies:
	docker-compose -f tools/docker-compose.yaml up
run: build
	./kong-control-plane