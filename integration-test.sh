#!/bin/bash
# Simple smoke test to see if echo service response through Envoy

./kong-control-plane &
kcpPid=$!

cd tools

trap "kill ${kcpPid} && docker-compose down" EXIT
docker-compose up -d

echo "Running the test"
for ((i=0; i<=120; i++))
do
   curl -x localhost:31000 http://http-echo.com/test
   exitCode=$?
   curl -x localhost:31000 -X GET http://specific-http-echo.com/specific
   specificExitCodeGET=$?
   curl -x localhost:31000 --head http://specific-http-echo.com/specific
   specificExitCodeHEAD=$?

   if [ $exitCode -eq 0 ] && [ $specificExitCodeGET -eq 0 ] && [ $specificExitCodeHEAD -eq 0 ]; then
    echo "Test passeed"
    exit 0
   fi
   sleep 1
done

echo "Test timeout out"
exit 1