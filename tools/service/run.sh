#!/usr/bin/env sh

set -o pipefail
set -o errexit

port=80
service_name=http-echo
ip="$(hostname -i)"

sleep 10

# cleanup
curl -i -X DELETE --fail --url http://kong:8001/routes/http-echo
curl -i -X DELETE --fail --url http://kong:8001/routes/specific-http-echo
curl -i -X DELETE --fail --url http://kong:8001/services/http-echo

# register service and routes
curl -i -X POST --fail --url http://kong:8001/services/ --data 'name=http-echo' --data "url=http://$ip"
curl -i -X POST --fail --url http://kong:8001/services/http-echo/routes  --data 'name=http-echo' --data 'hosts[]=http-echo.com'
curl -i -X POST --fail --url http://kong:8001/services/http-echo/routes  --data 'methods[]=GET&methods[]=HEAD' --data 'name=specific-http-echo' --data 'hosts[]=specific-http-echo.com' --data 'paths[]=/specific'

cd /app
node ./index.js

