package state

import (
	"log"
	"reflect"
	"sync"
	"time"
)

type Updater struct {
	Source   Source
	Target   Target
	previous State
	running  bool
	stopCh   chan bool
}

func (s *Updater) Start(wg *sync.WaitGroup, interval time.Duration) {
	s.stopCh = make(chan bool, 1)
	log.Printf("Start updating the snapshot every %v seconds", interval.Seconds())
	go func() {
		defer wg.Done()
		ticker := time.NewTicker(interval)
		for {
			select {
			case <-s.stopCh:
				log.Println("The snapshot updater is stopped")
				return
			case <-ticker.C:
				s.update()
			}
		}
	}()
}

func (s *Updater) Stop() {
	log.Println("Stopping the snapshot updater")
	s.stopCh <- true
}

func (s *Updater) update() {
	state, err := s.Source.State()
	if err != nil {
		log.Printf("Error while getting the service: %v", err)
	} else if reflect.DeepEqual(state, s.previous) {
		log.Println("States are the same. No need to update the snapshot")
	} else {
		log.Printf("Saving the new state: %v", state)
		err := s.Target.Save(state)
		if err != nil {
			log.Printf("Error while setting the snapshot: %v", err)
		}
		s.previous = state
	}
}
