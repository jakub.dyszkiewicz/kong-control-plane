package state

import "time"

type Service struct {
	ID                string
	Name              string
	Host              string
	Port              uint32
	Routes            []Route
	ConnectionTimeout time.Duration
}

type Route struct {
	Name    string
	Paths   []string
	Domains []string
	Methods []string
}

type State struct {
	Services []Service
}

type Source interface {
	State() (State, error)
}

type Target interface {
	Save(state State) error
}
