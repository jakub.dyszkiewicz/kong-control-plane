package state

import (
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
	"time"
)

type TestTarget struct {
	saved State
	savedTimes int
}

func (t *TestTarget) Save(state State) error {
	t.saved = state
	t.savedTimes++
	return nil
}

type TestSource struct {
	state State
}

func (t *TestSource) State() (State, error) {
	return t.state, nil
}

func TestUpdateTheSnapshotOnlyWhenItsDifferent(t *testing.T) {
	// given
	source := TestSource{}
	target := TestTarget{}
	updater := Updater{
		Target: &target,
		Source: &source,
	}

	// when provider generates new state
	source.state = stateWithService("serv1")
	updater.update()

	// then the state should be passes to target
	assert.EqualValues(t, stateWithService("serv1"), target.saved)
	assert.Equal(t, 1, target.savedTimes)

	// when source generates the same state
	updater.update()

	// then the state should not be passed to target
	assert.Equal(t, 1, target.savedTimes)
}

func TestUpdateTheSnapshotOnGivenInterval(t *testing.T) {
	// given
	provider := TestSource{}
	updateFn := TestTarget{}
	updater := Updater{
		Target: &updateFn,
		Source: &provider,
	}

	wg := sync.WaitGroup{}
	wg.Add(1)

	interval := 10 * time.Millisecond

	// when updater start and new state is set
	updater.Start(&wg, interval)
	provider.state = stateWithService("serv1")

	// then after 2 intervals the state should be saved
	time.Sleep(interval * 2)
	assert.Equal(t, 1, updateFn.savedTimes)

	// when
	provider.state = stateWithService("serv2")

	// then
	time.Sleep(interval * 2)
	assert.Equal(t, 2, updateFn.savedTimes)

	// graceful cleanup
	updater.Stop()
	wg.Wait()
}

func stateWithService(name string) State {
	return State{
		[]Service{
			{
				Name: name,
			},
		},
	}
}