package envoy

import (
	"github.com/envoyproxy/go-control-plane/envoy/api/v2"
	"github.com/envoyproxy/go-control-plane/envoy/api/v2/core"
	"github.com/envoyproxy/go-control-plane/envoy/api/v2/endpoint"
	route2 "github.com/envoyproxy/go-control-plane/envoy/api/v2/route"
	"github.com/envoyproxy/go-control-plane/pkg/cache"
	"github.com/jakubdyszkiewicz/kong-control-plane/state"
	"math/rand"
	"strconv"
	"strings"
)

func newSnapshot(state state.State) *cache.Snapshot {
	var virtualHosts []route2.VirtualHost
	var clusters, endpoints, routes, listeners []cache.Resource
	for _, service := range state.Services {
		cluster := newCluster(service)
		clusters = append(clusters, cluster)

		endpoint := newEndpoint(service)
		endpoints = append(endpoints, endpoint)

		for _, route := range service.Routes {
			virtualHost := newRoute(service, route)
			virtualHosts = append(virtualHosts, virtualHost)
		}
	}
	route := v2.RouteConfiguration{
		Name:         "default_routes",
		VirtualHosts: virtualHosts,
	}
	routes = append(routes, &route)
	version := strconv.Itoa(rand.Int())
	snapshot := cache.NewSnapshot(version, endpoints, clusters, routes, listeners)
	return &snapshot
}

func newCluster(service state.Service) *v2.Cluster {
	return &v2.Cluster{
		Name: service.Name,
		ClusterDiscoveryType: &v2.Cluster_Type{
			Type: v2.Cluster_EDS,
		},
		EdsClusterConfig: &v2.Cluster_EdsClusterConfig{
			EdsConfig: &core.ConfigSource{
				ConfigSourceSpecifier: &core.ConfigSource_Ads{
					Ads: &core.AggregatedConfigSource{},
				},
			},
		},
		ConnectTimeout: service.ConnectionTimeout,
	}
}

func newEndpoint(service state.Service) *v2.ClusterLoadAssignment {
	return &v2.ClusterLoadAssignment{
		ClusterName: service.Name,
		Endpoints: []endpoint.LocalityLbEndpoints{
			{
				LbEndpoints: []endpoint.LbEndpoint{
					{
						HostIdentifier: &endpoint.LbEndpoint_Endpoint{
							Endpoint: &endpoint.Endpoint{
								Address: &core.Address{
									Address: &core.Address_SocketAddress{
										SocketAddress: &core.SocketAddress{
											Protocol: core.TCP,
											Address:  service.Host,
											PortSpecifier: &core.SocketAddress_PortValue{
												PortValue: service.Port,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
}

func newRoute(service state.Service, route state.Route) route2.VirtualHost {
	var routes []route2.Route
	if len(route.Paths) == 0 {
		routes = append(routes, forwardAllToClusterRoute(route.Methods, service))
	}
	for _, path := range route.Paths {
		routes = append(routes, forwardToClusterRoute(path, route.Methods, service))
	}
	return route2.VirtualHost{
		Name:    service.Name,
		Domains: route.Domains,
		Routes:  routes,
	}
}

func forwardToClusterRoute(route string, methods []string, service state.Service) route2.Route {
	return route2.Route{
		Match: route2.RouteMatch{
			PathSpecifier: &route2.RouteMatch_Path{
				Path: route,
			},
			Headers: methodHeadersMatchers(methods),
		},
		Action: &route2.Route_Route{
			Route: &route2.RouteAction{
				ClusterSpecifier: &route2.RouteAction_Cluster{
					Cluster: service.Name,
				},
			},
		},
	}
}

func forwardAllToClusterRoute(methods []string, service state.Service) route2.Route {
	return route2.Route{
		Match: route2.RouteMatch{
			PathSpecifier: &route2.RouteMatch_Prefix{
				Prefix: "/",
			},
			Headers: methodHeadersMatchers(methods),
		},
		Action: &route2.Route_Route{
			Route: &route2.RouteAction{
				ClusterSpecifier: &route2.RouteAction_Cluster{
					Cluster: service.Name,
				},
			},
		},
	}
}

func methodHeadersMatchers(methods []string) []*route2.HeaderMatcher {
	if len(methods) == 0 {
		return nil
	}
	regex := strings.Join(methods, "|")
	return []*route2.HeaderMatcher{
		{
			Name: ":method",
			HeaderMatchSpecifier: &route2.HeaderMatcher_RegexMatch{
				RegexMatch: regex,
			},
		},
	}
}
