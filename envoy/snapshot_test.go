package envoy

import (
	"github.com/envoyproxy/go-control-plane/envoy/api/v2"
	"github.com/envoyproxy/go-control-plane/envoy/api/v2/core"
	"github.com/envoyproxy/go-control-plane/envoy/api/v2/endpoint"
	"github.com/envoyproxy/go-control-plane/envoy/api/v2/route"
	"github.com/jakubdyszkiewicz/kong-control-plane/state"
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	service = state.Service{
		ID:                "00000000-0000-0000-0000-000000000001",
		Name:              "test-service1",
		Host:              "127.0.0.1",
		Port:              1234,
		ConnectionTimeout: 60000,
		Routes: []state.Route{
			{
				Name:    "test-service1",
				Domains: []string{"test-service1.com"},
			},
		},
	}
	testState = state.State{
		Services: []state.Service{
			service,
		},
	}
)

func TestShouldGenerateEdsFromState(t *testing.T) {
	// when
	snapshot := newSnapshot(testState)

	// then should generate eds
	assert.Len(t, snapshot.Endpoints.Items, 1)

	expectedLoadAssignment := v2.ClusterLoadAssignment{
		ClusterName: "test-service1",
		Endpoints: []endpoint.LocalityLbEndpoints{
			{
				LbEndpoints: []endpoint.LbEndpoint{
					{
						HostIdentifier: &endpoint.LbEndpoint_Endpoint{
							Endpoint: &endpoint.Endpoint{
								Address: &core.Address{
									Address: &core.Address_SocketAddress{
										SocketAddress: &core.SocketAddress{
											Protocol: core.TCP,
											Address:  "127.0.0.1",
											PortSpecifier: &core.SocketAddress_PortValue{
												PortValue: 1234,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}

	assert.EqualValues(t, &expectedLoadAssignment, snapshot.Endpoints.Items["test-service1"])
}

func TestGenerateCdsFromState(t *testing.T) {
	// when
	snapshot := newSnapshot(testState)

	// then should generate cds
	assert.Len(t, snapshot.Endpoints.Items, 1)

	expectedClusters := &v2.Cluster{
		Name: "test-service1",
		ClusterDiscoveryType: &v2.Cluster_Type{
			Type: v2.Cluster_EDS,
		},
		EdsClusterConfig: &v2.Cluster_EdsClusterConfig{
			EdsConfig: &core.ConfigSource{
				ConfigSourceSpecifier: &core.ConfigSource_Ads{
					Ads: &core.AggregatedConfigSource{},
				},
			},
		},
		ConnectTimeout: 60000,
	}

	assert.EqualValues(t, expectedClusters, snapshot.Clusters.Items["test-service1"])
}

func TestGenerateRdsFromState(t *testing.T) {
	// when
	snapshot := newSnapshot(testState)

	// then should generate rds
	assert.Len(t, snapshot.Endpoints.Items, 1)

	expectedRoutes := &v2.RouteConfiguration{
		Name: "default_routes",
		VirtualHosts: []route.VirtualHost{
			{
				Name:    "test-service1",
				Domains: []string{"test-service1.com"},
				Routes: []route.Route{
					{
						Match: route.RouteMatch{
							PathSpecifier: &route.RouteMatch_Prefix{
								Prefix: "/",
							},
						},
						Action: &route.Route_Route{
							Route: &route.RouteAction{
								ClusterSpecifier: &route.RouteAction_Cluster{
									Cluster: service.Name,
								},
							},
						},
					},
				},
			},
		},
	}
	assert.EqualValues(t, expectedRoutes, snapshot.Routes.Items["default_routes"])
}