package envoy

import (
	"fmt"
	api "github.com/envoyproxy/go-control-plane/envoy/api/v2"
	discovery "github.com/envoyproxy/go-control-plane/envoy/service/discovery/v2"
	xds "github.com/envoyproxy/go-control-plane/pkg/server"
	"google.golang.org/grpc"
	"log"
	"net"
	"sync"
)

type Server struct {
	grpcServer *grpc.Server
}

func (s *Server) Start(wg *sync.WaitGroup, port int) {
	server := xds.NewServer(snapshotCache, nil)
	s.grpcServer = grpc.NewServer()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Printf("Failed to start Server on port %d: %v", port, err)
	}

	discovery.RegisterAggregatedDiscoveryServiceServer(s.grpcServer, server)
	api.RegisterEndpointDiscoveryServiceServer(s.grpcServer, server)
	api.RegisterClusterDiscoveryServiceServer(s.grpcServer, server)
	api.RegisterRouteDiscoveryServiceServer(s.grpcServer, server)
	api.RegisterListenerDiscoveryServiceServer(s.grpcServer, server)

	log.Printf("Starting the XDS server on %v port", port)
	go func() {
		defer wg.Done()
		if err := s.grpcServer.Serve(lis); err != nil {
			fmt.Printf("Failed to start Control Plane: %v", err)
		}
		log.Println("The xds server is stopped")
	}()
}

func (s *Server) Stop() {
	log.Println("Stopping the xds server")
	if s.grpcServer != nil {
		s.grpcServer.Stop()
	}
}
