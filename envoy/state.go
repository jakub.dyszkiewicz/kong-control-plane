package envoy

import (
	"github.com/envoyproxy/go-control-plane/pkg/cache"
	"github.com/jakubdyszkiewicz/kong-control-plane/state"
	"google.golang.org/grpc/grpclog"
	"os"
)

var (
	snapshotCache = cache.NewSnapshotCache(true, group{}, grpclog.NewLoggerV2(os.Stdout, os.Stdout, os.Stderr))
)

type Target struct {
}

func (e *Target) Save(s state.State) error {
	snapshot := newSnapshot(s)
	return snapshotCache.SetSnapshot(nodeID, *snapshot)
}
