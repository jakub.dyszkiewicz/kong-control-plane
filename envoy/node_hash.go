package envoy

import "github.com/envoyproxy/go-control-plane/envoy/api/v2/core"

const (
	nodeID = "node"
)

type group struct{}

func (group) ID(node *core.Node) string {
	return nodeID
}
