# Kong Control Plane 🦍

The Control Plane that integrates with [Kong](https://konghq.com) and push changes to [Envoy](https://www.envoyproxy.io).

## Architecture & Implementation Overview
The application is implemented in a "ports & adapters"-like architecture. Having this coded in this fashion it's really easy
to add or stub a source or a target of a discovery service state data.

#### State module
State module contains common structures and the application logic. It does not know anything about Envoy nor Kong.

* `State`, `Service`, `Route` - represents the service discovery data
* `Source` -  the source that provides the state on demand.
* `Target` - the destination that receives the state

The main application logic is to pull the `State` from `Source` periodically and pass it to `Target` when it's changed.
This process is represented in `Updater` struct.

#### Kong module
It implements the `Source` interface.
The `Client` downloads the data from Kong and converts it into `State` in `state.go`. It support Kong
pagination offsets.

#### Envoy module
It implements the `Target` interface. It uses [go-control-plane](https://github.com/envoyproxy/go-control-plane/)
project to provide integration with Envoy.

When new `State` is set, it is converted to Envoy's structures and set in `go-control-plane`'s `SnapshotCache`.

To sent the xDS data to Envoys, the xDS server (`server.go`) must be started. The server then tracks `SnapshotCache`
for changes.

## Requirements
* Go 1.12
* Docker Compose

## Build
To build a project run
```bash
make build
```

## Run
### Dependencies
The Control Plane requires Kong to run and sends the config to connected Envoys. Run the docker compose with:
```bash
make run-dependencies
```
It will start:
* Kong on `http://localhost:8001`
* Echo service that will be registered in Kong under `http-echo` service name with `http-echo.com` route and `specific-http-echo.com` route with only `/specific` path and `GET` method
* Envoy with the listener on `http://localhost:31000` and admin on `http://localhost:9999` 

### Control Plane
After the build, you can run a project with
```bash
make run
```

### Verify
Within a couple of seconds, the Envoy should have the state of the Kong.
You can check whether you can call the service through the Envoy with:
```bash
curl -x localhost:31000 http://http-echo.com/test -v
``` 
You should see the output
```bash
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 31000 (#0)
> GET http://http-echo.com/test HTTP/1.1
> Host: http-echo.com
> User-Agent: curl/7.54.0
> Accept: */*
> Proxy-Connection: Keep-Alive
>
< HTTP/1.1 200 OK
< x-powered-by: Express
< content-type: application/json; charset=utf-8
< content-length: 536
< etag: W/"218-ZRfyiKJiIw7YPHChM5uwa/MfUIk"
< date: Mon, 18 Mar 2019 20:59:30 GMT
< x-envoy-upstream-service-time: 11
< server: envoy
<
{
  "path": "/test",
  "headers": {
    "host": "http-echo.com",
    "user-agent": "curl/7.54.0",
    "accept": "*/*",
    "x-forwarded-proto": "http",
    "x-request-id": "4250cbad-1c6c-4b7c-a0ac-bd15a047231e",
    "x-envoy-expected-rq-timeout-ms": "15000",
    "content-length": "0"
  },
  "method": "GET",
  "body": "",
  "fresh": false,
  "hostname": "http-echo.com",
  "ip": "::ffff:192.168.112.2",
  "ips": [],
  "protocol": "http",
  "query": {},
  "subdomains": [],
  "xhr": false,
  "os": {
    "hostname": "9d97a4a799c3"
  }
* Connection #0 to host localhost left intact
}
```

To check if specific path works run
```bash
curl -x localhost:31000 http://specific-http-echo.com/specific -v
``` 

## Test
### Unit tests
To check unit tests run:
```bash
make test
```
### Integration test
To check the integration test run:
```bash
make int-test
```
It will run the docker compose and kong-control-plane and see if echo is available through Envoy.


## Config
You can configure the control plane with env variables
* `KCP_KONG_ADDRESS` - Default: `http://localhost:8001`. The address of Kong
* `KCP_KONG_CONNECTION_TIMEOUT` - Default: `10s`. The connection timeout to Kong
* `KCP_KONG_READ_TIMEOUT` - Default: `1s`. The read timeout to Kong
* `KCP_SNAPSHOT_UPDATER_INTERVAL` - Default: `5s`. Interval of reading the state from Kong and sending it to Envoy if needed.
* `KCP_XDS_SERVER_PORT` - Default: `40000`. The port of XDS server.

## Other
* The changes are sent using [ADS](https://www.envoyproxy.io/docs/envoy/latest/configuration/overview/v2_overview#aggregated-discovery-service)
* You can only send the routes with the IP over xDS. There is an [issue on Github](https://github.com/envoyproxy/go-control-plane/issues/87) on it.
 If there is a need to support hostnames, you have to resolve it using DNS on Control Plane side.
* Right now Control Plane pulls Kong state every 5 seconds (or other if configured). Ideally, 
 there should be an API that Control Plane can connect to and Kong would push the changes.
 This would reduce the latency of changes propagation.