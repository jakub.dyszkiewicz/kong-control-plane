package kong

import (
	"github.com/jakubdyszkiewicz/kong-control-plane/state"
	"time"
)

type Source struct {
	Client Client
}

func (s *Source) State() (state.State, error) {
	services, err := s.services()
	if err != nil {
		return state.State{}, err
	}

	routes, err := s.routes()
	if err != nil {
		return state.State{}, err
	}

	connectRoutesToServices(services, routes)
	return state.State{
		Services: services,
	}, nil
}

func connectRoutesToServices(services []state.Service, routes map[string][]state.Route) {
	for i, service := range services {
		if routesForService, ok := routes[service.ID]; ok {
			service.Routes = routesForService
			services[i] = service
		}
	}
}

func (s *Source) services() ([]state.Service, error) {
	responsesData, err := s.fetchAllServices()
	if err != nil {
		return nil, err
	}
	return convertToServices(responsesData), nil
}

// fetches all services by taking into account the returned offset
func (s *Source) fetchAllServices() ([]ServicesData, error) {
	var responsesData []ServicesData
	var offset string
	for {
		response, err := s.Client.Services(offset)
		if err != nil {
			return nil, err
		}
		responsesData = append(responsesData, response.Data...)
		if response.Next == "" {
			break
		}
		offset = response.Next
	}
	return responsesData, nil
}

func convertToServices(responsesData []ServicesData) []state.Service {
	var services []state.Service
	for _, respService := range responsesData {
		service := state.Service{
			ID:                respService.ID,
			Name:              respService.Name,
			Host:              respService.Host,
			Port:              respService.Port,
			ConnectionTimeout: time.Duration(respService.ConnectionTimeout) * time.Millisecond,
		}
		services = append(services, service)
	}
	return services
}

func (s *Source) routes() (map[string][]state.Route, error) {
	responsesData, err := s.fetchAllRoutes()
	if err != nil {
		return nil, err
	}
	return convertToRoutes(responsesData), nil
}

// fetches all routes by taking into account the returned offset
func (s *Source) fetchAllRoutes() ([]RoutesData, error) {
	var responsesData []RoutesData
	var offset string
	for {
		response, err := s.Client.Routes(offset)
		if err != nil {
			return nil, err
		}
		responsesData = append(responsesData, response.Data...)
		if response.Next == "" {
			break
		}
		offset = response.Next
	}
	return responsesData, nil
}

func convertToRoutes(responsesData []RoutesData) map[string][]state.Route {
	routes := map[string][]state.Route{}
	for _, resRoute := range responsesData {
		route := state.Route{
			Name:    resRoute.Name,
			Paths:   resRoute.Paths,
			Domains: resRoute.Hosts,
			Methods: resRoute.Methods,
		}
		cur, ok := routes[resRoute.Service.ID]
		if ! ok {
			routes[resRoute.Service.ID] = []state.Route{}
		}
		routes[resRoute.Service.ID] = append(cur, route)
	}
	return routes
}
