package kong

import (
	"github.com/jakubdyszkiewicz/kong-control-plane/state"
	"github.com/stretchr/testify/assert"
	"testing"
)

type OffsetTestClient struct {
}

func (c *OffsetTestClient) Services(offset string) (ServicesResponse, error) {
	if offset == "" {
		return ServicesResponse{
			Data: []ServicesData{
				{
					ID: "serv1",
				},
			},
			Next: "1",
		}, nil
	}
	if offset == "1" {
		return ServicesResponse{
			Data: []ServicesData{
				{
					ID: "serv2",
				},
			},
			Next: "",
		}, nil
	}
	return ServicesResponse{}, nil
}

func (c *OffsetTestClient) Routes(offset string) (RoutesResponse, error) {
	if offset == "" {
		return RoutesResponse{
			Data: []RoutesData{
				{
					Name: "route1",
					Service: ServiceID{
						ID: "serv1",
					},
				},
			},
			Next: "1",
		}, nil
	}
	if offset == "1" {
		return RoutesResponse{
			Data: []RoutesData{
				{
					Name: "route2",
					Service: ServiceID{
						ID: "serv2",
					},
				},
			},
			Next: "",
		}, nil
	}
	return RoutesResponse{}, nil
}

func TestShouldFetchAllDataUsingOffsets(t *testing.T) {
	// given
	stateProvider := Source{
		Client: &OffsetTestClient{},
	}

	// when
	providedState, err := stateProvider.State()

	// then
	assert.Nil(t, err)
	expectedState := state.State{
		Services: []state.Service{
			{
				ID: "serv1",
				Routes: []state.Route{
					{
						Name: "route1",
					},
				},
			},
			{
				ID: "serv2",
				Routes: []state.Route{
					{
						Name: "route2",
					},
				},
			},
		},
	}
	assert.EqualValues(t, providedState, expectedState)
}
