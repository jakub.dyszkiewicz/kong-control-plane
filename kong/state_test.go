package kong

import (
	"testing"
	"time"
)
import "github.com/stretchr/testify/assert"

type TestClient struct {
	servicesResponse ServicesResponse
	routesResponse   RoutesResponse
}

func (c *TestClient) Services(offset string) (ServicesResponse, error) {
	return c.servicesResponse, nil
}

func (c *TestClient) Routes(offset string) (RoutesResponse, error) {
	return c.routesResponse, nil
}

func TestStateIsGenerated(t *testing.T) {
	// given
	servResponse := ServicesResponse{
		Data: []ServicesData{
			{
				ID:                "00000000-0000-0000-0000-000000000001",
				Name:              "test-service1",
				Host:              "127.0.0.1",
				Port:              1234,
				ConnectionTimeout: 60000,
			},
			{
				ID:                "00000000-0000-0000-0000-000000000002",
				Name:              "test-service2",
				Host:              "127.0.0.2",
				Port:              1234,
				ConnectionTimeout: 60000,
			},
		},
	}

	routesResponse := RoutesResponse{
		Data: []RoutesData{
			{
				Name:    "10000000-0000-0000-0000-000000000001",
				Hosts:   []string{"test-service1.com"},
				Methods: []string{"GET"},
				Paths:   []string{"/some-path"},
				Service: ServiceID{ID: "00000000-0000-0000-0000-000000000001"},
			},
			{
				Name:    "10000000-0000-0000-0000-000000000002",
				Hosts:   []string{"test-service2.com"},
				Methods: nil,
				Paths:   nil,
				Service: ServiceID{ID: "00000000-0000-0000-0000-000000000002"},
			},
		},
	}

	testClient := TestClient{
		servicesResponse: servResponse,
		routesResponse:   routesResponse,
	}

	stateProvider := Source{
		Client: &testClient,
	}

	// when
	state, err := stateProvider.State()

	// then
	assert.Nil(t, err)
	assert.Len(t, state.Services, 2)

	// should connect create first service from the response
	assert.Equal(t, state.Services[0].Name, servResponse.Data[0].Name)
	assert.Equal(t, state.Services[0].ID, servResponse.Data[0].ID)
	assert.Equal(t, state.Services[0].Host, servResponse.Data[0].Host)
	assert.Equal(t, state.Services[0].Port, servResponse.Data[0].Port)
	assert.Equal(t, state.Services[0].ConnectionTimeout, time.Millisecond*time.Duration(servResponse.Data[0].ConnectionTimeout))

	// should connect first route to the first response
	assert.Len(t, state.Services[0].Routes, 1)
	assert.Equal(t, state.Services[0].Routes[0].Name, routesResponse.Data[0].Name)
	assert.Equal(t, state.Services[0].Routes[0].Paths, routesResponse.Data[0].Paths)
	assert.Equal(t, state.Services[0].Routes[0].Domains, routesResponse.Data[0].Hosts)

	// should connect create second service from the response
	assert.Equal(t, state.Services[1].Name, servResponse.Data[1].Name)
	assert.Equal(t, state.Services[1].ID, servResponse.Data[1].ID)
	assert.Equal(t, state.Services[1].Host, servResponse.Data[1].Host)
	assert.Equal(t, state.Services[1].Port, servResponse.Data[1].Port)
	assert.Equal(t, state.Services[1].ConnectionTimeout, time.Millisecond*time.Duration(servResponse.Data[1].ConnectionTimeout))

	// should connect second route to the second response
	assert.Len(t, state.Services[1].Routes, 1)
	assert.Equal(t, state.Services[1].Routes[0].Name, routesResponse.Data[1].Name)
	assert.Equal(t, state.Services[1].Routes[0].Paths, routesResponse.Data[1].Paths)
	assert.Equal(t, state.Services[1].Routes[0].Domains, routesResponse.Data[1].Hosts)
}
