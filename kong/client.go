package kong

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

type Client interface {
	Services(offset string) (ServicesResponse, error)
	Routes(offset string) (RoutesResponse, error)
}

type ServicesResponse struct {
	Data []ServicesData `json:"data"`
	Next string         `json:"next"`
}

type ServicesData struct {
	ID                string `json:"id"`
	Name              string `json:"name"`
	Host              string `json:"host"`
	Port              uint32 `json:"port"`
	ConnectionTimeout int    `json:"connect_timeout"`
}

type RoutesResponse struct {
	Data []RoutesData `json:"data"`
	Next string       `json:"next"`
}

type RoutesData struct {
	Name    string    `json:"name"`
	Hosts   []string  `json:"hosts"`
	Methods []string  `json:"methods"`
	Paths   []string  `json:"paths"`
	Service ServiceID `json:"service"`
}

type ServiceID struct {
	ID string `json:"id"`
}

type HttpClient struct {
	url        string
	httpClient http.Client
}

func NewClient(url string, connectionTimeout time.Duration, readTimeout time.Duration) Client {
	return &HttpClient{
		url: url,
		httpClient: http.Client{
			Transport: &http.Transport{
				DialContext: func(ctx context.Context, network, addr string) (conn net.Conn, e error) {
					return net.DialTimeout(network, addr, connectionTimeout)
				},
			},
			Timeout: readTimeout,
		},
	}
}

func (c *HttpClient) Services(offset string) (ServicesResponse, error) {
	var servicesResponse = ServicesResponse{}
	resp, err := c.httpClient.Get(c.url + "/services" + offsetParamUrl(offset))
	if err != nil {
		return servicesResponse, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return servicesResponse, err
	}
	err = json.Unmarshal(body, &servicesResponse)
	if err != nil {
		return servicesResponse, err
	}
	return servicesResponse, nil
}

func (c *HttpClient) Routes(offset string) (RoutesResponse, error) {
	var routesResponse = RoutesResponse{}
	resp, err := c.httpClient.Get(c.url + "/routes" + offsetParamUrl(offset))
	if err != nil {
		return routesResponse, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return routesResponse, err
	}
	err = json.Unmarshal(body, &routesResponse)
	if err != nil {
		return routesResponse, err
	}
	return routesResponse, nil
}

func offsetParamUrl(offset string) string {
	if offset != "" {
		return "?offset=" + offset
	}
	return ""
}
